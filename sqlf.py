#!/usr/bin/env python
# -*- coding: utf-8 -*
import argparse
from functools import reduce

import sqlparse


def read_file(file_):
    with open(file_, "r") as inhandle:
        input = inhandle.read()
    return input


def prepend_comma(str_):
    """Add a comma to the front of a string while preserving any leading indentation"""
    # calculate existing intendation, subtract 2 to leave room to add comma and space
    indent = len(str_) - len(str_.lstrip()) - 2
    # add indent spaces and comma to front of line
    indent_comma = indent * " " + ", "
    str_ = indent_comma + str_.lstrip()
    return str_


def commas_first(sql):
    """Loop over lines of a `sql` string, moving trailing commas to the front as needed"""
    lines = sql.split("\n")
    for line in lines:
        try:
            if line[-1] == ",":  # find lines ending with comma
                idx = lines.index(line)
                line = line[:-1]  # remove comma from end of line
                lines[idx] = line  # replace original line
                next_line = lines[idx + 1]
                next_line = prepend_comma(next_line)  # add comma to front of next line
                lines[idx + 1] = next_line  # replace
        except IndexError:
            pass
    # put string back together
    sql = "\n".join(lines)
    return sql


def cleanup(sql):
    """Various adjustments to default `sqlformat` output"""
    # start select after union on new line
    sql = sql.replace("union select", "union\nselect")
    # ensure semicolon on own line - start new statements on new line
    sql = sql.replace(";", "\n;\n")

    # strip trailing whitespace from lines (this should probably always come last)
    sql = "\n".join(line.rstrip() for line in sql.split("\n"))

    return sql


def prettify(input):
    """Use standard sqlparse.format on `input`, then adjust
    to better conform to BenchPrep style guide

    Args:
    input (str): The SQL input to be formatted

    Returns:
        str: The formatted SQL string
    """
    # Get basic right-aligned format from sqlparse built-in
    fmtd = sqlparse.format(input, reindent_aligned=True)
    # Coerce entire formatted string to lowercase
    fmtd = fmtd.lower()
    # Call `commas_first()` on sqlformat result
    sql = commas_first(fmtd)
    # Additional tweaks
    sql = cleanup(sql)
    return sql


def uglify(input):
    """Use sqlparse.format to strip comments, compress out whitespace,
    returning a single long string without comments or linebreaks.
    Handy for automated submission of SQL in various scenarios
    """
    fmtd = " ".join(sqlparse.format(input, strip_comments=True).split())
    repls = (" ,", ","), ("( ", "("), (" )", ")")
    fmtd = reduce(lambda a, kv: a.replace(*kv), repls, fmtd)
    return fmtd


def write_file(file_, sql):
    with open(file_, "w") as outhandle:
        outhandle.write(sql)


def handle_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("filename")
    parser.add_argument("-p", "--pretty", action="store_true", default=True)
    parser.add_argument("-u", "--ugly", action="store_true", default=False)
    args = parser.parse_args()
    return args


def main():
    args = handle_args()
    file_ = args.filename

    input = read_file(file_)
    sql = prettify(input)
    if args.ugly:
        sql = uglify(input)
    write_file(file_, sql)


if __name__ == "__main__":
    main()
